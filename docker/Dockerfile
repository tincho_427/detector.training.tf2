FROM tensorflow/tensorflow:latest-gpu

RUN apt update --fix-missing && \
    apt-get install --no-install-recommends -y \
    vim \
    protobuf-compiler \
    # required by 'object_detection'
    ffmpeg \
    libsm6 \
    libxext6

ENV PROJECT_DIR='/dl/detectors/training_tf2'
RUN mkdir -p $PROJECT_DIR/packages
WORKDIR $PROJECT_DIR

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

COPY packages/tensorflow_models $PROJECT_DIR/packages/tensorflow_models
# Install Object Detection API
RUN cd $PROJECT_DIR/packages/tensorflow_models/research && \
    cp object_detection/packages/tf2/setup.py . && \
    python -m pip install --use-feature=2020-resolver .
ENV PYTHONPATH=$PYTHONPATH:$PROJECT_DIR/packages/tensorflow_models/research:$PROJECT_DIR/packages/tensorflow_models/research/slim

# Install protobuf
RUN cd $PROJECT_DIR/packages/tensorflow_models/research  && \
    protoc object_detection/protos/*.proto --python_out=.

COPY . $PROJECT_DIR
