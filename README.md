# Object Detection Training using TensorFlow 2.11 (also tested with 2.9)

Tensorflow 2.11 requires:
  	* Python 3.7-3.10
    * for run on GPU, CUDA 11.2, cuDNN  8.1 (it was tested with newer versions of CUDA and works)

## Installation

Tensorflow has different problems recognizing GPU inside docker, because of CUDA drivers.
Is because of that that we are going to use a virtualenv.

```sh
# create a virtualenv with mkvirtualenvwrapper
cd detectors_training_tf2
mkvirtualenv --python=3.7 p37_tf211
pip3 install -r requirements.txt
```

Clone the tensorflow models git repository & Install TensorFlow Object Detection API:
```sh
mkdir -p packages
git clone --q https://github.com/tensorflow/models.git
cd models/research

# Compile protos.
protoc object_detection/protos/*.proto --python_out=.

# Install TensorFlow Object Detection API.
cp object_detection/packages/tf2/setup.py .
python3 -m pip install .
```

Testing the model builder (correct installed object detection API):
```sh
python3 object_detection/builders/model_builder_tf2_test.py
```

## Annotate images

### Create the annotations in .xml format

For example, from `plates_1376` took first 50 images for training and 10 images for testing. Put that images on the workspace folder.

```sh
# on a virtualenv (outside docker) because serve a UI.
mkvirtualenv training
pip3 install labelImg==1.8.5
labelImg workspace/training_demo/images/

# if you had only one folder and have to partinionate the data in train/data you could use:
python3 scripts/partition_dataset.py -x -i workspace/training_demo/images -r 0.1
```

### Convert the annotations in .xml format to .record (`TFRecord` format)

**NOTE**: **maybe** have to fix:
```sh
vim /usr/local/lib/python3.6/dist-packages/object_detection/utils/label_map_util.py  # line 132
# Replace by:
# with tf.io.gfile.GFile(path, 'r') as fid:
```

```sh
# inside docker
# license-plate
python3 scripts/generate_tfrecord.py -x workspace/training_demo/images/train -l workspace/training_demo/annotations/label_map.pbtxt -o workspace/training_demo/annotations/train.record
python3 scripts/generate_tfrecord.py -x workspace/training_demo/images/test -l workspace/training_demo/annotations/label_map.pbtxt -o workspace/training_demo/annotations/test.record

# pills
python3 scripts/generate_tfrecord.py -x workspace/pill_test/images/train -l workspace/pill_test/annotations/label_map.pbtxt -o workspace/pill_test/annotations/train.record
python3 scripts/generate_tfrecord.py -x workspace/pill_test/images/test -l workspace/pill_test/annotations/label_map.pbtxt -o workspace/pill_test/annotations/test.record
```

## Prepare for training

> The example for license-plate and pills are in the repository, so you could skip this step.

Prepare the `pipeline.config` file with the configuration for training and get the pre-trained model.

Download a pre-trained model from [tf2_detection_zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md):

```sh
# example with ssd_mobilenet_v2_320x320_coco17_tpu-8 (for license-plate)

# download pre-trained model
cd workspace/training_demo/pre-trained-models
wget http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8.tar.gz
tar -xzvf ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8.tar.gz
rm ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8.tar.gz
cd ..

# configure training pipeline
mkdir -p models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8
cp pre-trained-models/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/pipeline.config models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/
# edit with the correct data:
vim models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/pipeline.config
# For example
#
# ssd.num_classes: 1
# ssd.image_resizer.fixed_shape_resizer.height: 640
# ssd.image_resizer.fixed_shape_resizer.width: 640
# train_config.fine_tune_checkpoint: "pre-trained-models/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/checkpoint/ckpt-0
# train_config.fine_tune_checkpoint_type: "detection"
# train_config.batch_size: 4  # Increase/Decrease this value depending on the available memory (Higher values require more memory and vice-versa)
# train_config.num_steps: 50000  # Number of steps to run
# train_input_reader.label_map_path: "annotations/label_map.pbtxt"
# train_input_reader.input_path: "annotations/train.record"
# eval_input_reader.label_map_path: "annotations/label_map.pbtxt"
# eval_input_reader.input_path: "annotations/test.record"
```

## Train the model

> Remember that if you add more images to make the **records** of the images!
> If you are using GPU and have more than one GPU available: `export CUDA_VISIBLE_DEVICES=0`

```sh
export CUDA_VISIBLE_DEVICES=0  # if are using gpu
# IMPORTANT! sometimes Tensorflow does not recognize GPU. If you run `import torch` previous to each tensorflow import, maybe will recognize it correctly. So, if it works, add that import at the top of each script used as 'model_main_tf2.py'.

# license-plate
python3 packages/models/research/object_detection/model_main_tf2.py \
    --model_dir=workspace/training_demo/models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8 \
    --pipeline_config_path=workspace/training_demo/models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/pipeline.config
    # --num_train_steps=50000 \
    --alsologtostderr

# pills
cd workspace/pill_test
python3 ../../packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --pipeline_config_path=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config
```

For monitoring the training of the model:
```sh
# in other terminal in the virtual env
pip3 install tensorboard
cd workspace/training_demo
tensorboard --logdir=models --port 6006
```

## Get model metrics (when is training)

```sh
docker exec -it detectors_training_tf2 bash
export CUDA_VISIBLE_DEVICES=1  # if are using gpu

# license-plate
python3 packages/models/research/object_detection/model_main_tf2.py \
  --model_dir=workspace/training_demo/models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8 \
  --pipeline_config_path=workspace/training_demo/models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/pipeline.config \
  # --checkpoint_dir=workspace/training_demo/models/license_plate_ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8 \
  # --num_train_steps=4000 \
  --alsologtostderr

# pills
cd /dl/detectors/training_tf2_vol/workspace/pill_test
python3 /dl/detectors/training_tf2_vol/packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --pipeline_config_path=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config --checkpoint_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --alsologtostderr
```

## Exporting inference graph

```sh
# pills
python3 packages/models/research/object_detection/exporter_main_v2.py \
  --input_type=image_tensor \
  --pipeline_config_path=workspace/pill_test/models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config \
  --trained_checkpoint_dir=workspace/pill_test/models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/ \
  --output_directory=workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8
# recommended to not forget the labels
cp workspace/pill_test/annotations/label_map.pbtxt workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model/
```

Test on an image:
```sh
python3 scripts/detect_on_image.py \
    --model=workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model \
    --labels=workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model/label_map.pbtxt \
    --image=workspace/pill_test/images/test/i-1e092ec6eabf47f9b85795a9e069181b.jpg \
    --threshold=0.5
```

-------------------------------------------------------------------------------

# Object Detection Training using TensorFlow 2.0 [+-deprecated]

## Installation

```sh
git submodule init
git submodule update
docker-compose -f docker/docker-compose.yml build
```

## Enter develop mode

```sh
docker-compose -f docker/docker-compose.yml up -d; docker exec -it detectors_training_tf2 bash
```

For testing installation:
```sh
# tf2
python -c "import tensorflow as tf;print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

## Annotate images

### Create the annotations in .xml format

For example, from `plates_1376` took first 50 images for training and 10 images for testing. Put that images on the workspace folder.

```sh
# on a virtualenv (outside docker) because serve a UI.
mkvirtualenv training
pip3 install labelImg==1.8.5
labelImg workspace/training_demo/images/

# if you had only one folder and have to partinionate the data in train/data you could use:
python3 scripts/partition_dataset.py -x -i workspace/training_demo/images -r 0.1
```

### Convert the annotations in .xml format to .record (`TFRecord` format)

**NOTE**: **maybe** have to fix:
```sh
vim /usr/local/lib/python3.6/dist-packages/object_detection/utils/label_map_util.py  # line 132
# Replace by:
# with tf.io.gfile.GFile(path, 'r') as fid:
```

```sh
# inside docker
# license-plate
python3 scripts/generate_tfrecord.py -x workspace/training_demo/images/train -l workspace/training_demo/annotations/label_map.pbtxt -o workspace/training_demo/annotations/train.record
python3 scripts/generate_tfrecord.py -x workspace/training_demo/images/test -l workspace/training_demo/annotations/label_map.pbtxt -o workspace/training_demo/annotations/test.record

# pills
python3 scripts/generate_tfrecord.py -x workspace/pill_test/images/train -l workspace/pill_test/annotations/label_map.pbtxt -o workspace/pill_test/annotations/train.record
python3 scripts/generate_tfrecord.py -x workspace/pill_test/images/test -l workspace/pill_test/annotations/label_map.pbtxt -o workspace/pill_test/annotations/test.record
```

## Prepare for training

> The example for license-plate and pills are in the repository, so you could skip this step.

Prepare the `pipeline.config` file with the configuration for training and get the pre-trained model.

Download a pre-trained model from [tf2_detection_zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md):

```sh
# example with ssd_mobilenet_v2_320x320_coco17_tpu-8 (for license-plate)

# download pre-trained model
cd workspace/training_demo/pre-trained-models
wget http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_320x320_coco17_tpu-8.tar.gz -O ssd_mobilenet_v2_320x320_coco17_tpu-8.tar.gz
tar -xzvf ssd_mobilenet_v2_320x320_coco17_tpu-8.tar.gz
rm ssd_mobilenet_v2_320x320_coco17_tpu-8.tar.gz
cd ..

# configure training pipeline
mkdir -p models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8
cp pre-trained-models/ssd_mobilenet_v2_320x320_coco17_tpu-8/pipeline.config models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8/
# edit with the correct data:
vim models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8/pipeline.config
# For example
#
# ssd.num_classes: 1
# train_config.batch_size: 8 # Increase/Decrease this value depending on the available memory (Higher values require more memory and vice-versa)
# fine_tune_checkpoint: "pre-trained-models/ssd_mobilenet_v2_320x320_coco17_tpu-8/
# fine_tune_checkpoint_type: "detection"
# train_input_reader.label_map_path: "annotations/label_map.pbtxt"
# train_input_reader.input_path: "annotations/train.record"
# eval_input_reader.label_map_path: "annotations/label_map.pbtxt"
# eval_input_reader.input_path: "annotations/test.record"
```

## Train the model

> Remember that if you add more images to make the **records** of the images!
> If you are using GPU and have more than one GPU available: `export CUDA_VISIBLE_DEVICES=0`

```sh
export CUDA_VISIBLE_DEVICES=0  # if are using gpu

# license-plate
cd workspace/training_demo
python3 ../../packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8 --pipeline_config_path=models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8/pipeline.config

# pills
cd workspace/pill_test
python3 ../../packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --pipeline_config_path=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config
```

For monitoring the training of the model:
```sh
# in other terminal in the virtual env
pip3 install tensorboard
cd workspace/training_demo
tensorboard --logdir=models --port 6006
```

## Get model metrics (when is training)

```sh
docker exec -it detectors_training_tf2 bash
export CUDA_VISIBLE_DEVICES=1  # if are using gpu

# license-plate
cd /dl/detectors/training_tf2_vol/workspace/training_demo
python3 /dl/detectors/training_tf2_vol/packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8 --pipeline_config_path=models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8/pipeline.config --checkpoint_dir=models/license_plate_ssd_mobilenet_v2_320x320_coco17_tpu-8 --alsologtostderr

# pills
cd /dl/detectors/training_tf2_vol/workspace/pill_test
python3 /dl/detectors/training_tf2_vol/packages/tensorflow_models/research/object_detection/model_main_tf2.py --model_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --pipeline_config_path=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config --checkpoint_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8 --alsologtostderr
```

## Exporting inference graph

```sh
# pills
cd /dl/detectors/training_tf2_vol/workspace/pill_test
python3 /dl/detectors/training_tf2_vol/packages/tensorflow_models/research/object_detection/exporter_main_v2.py --input_type=image_tensor --pipeline_config_path=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/pipeline.config --trained_checkpoint_dir=models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/ --output_directory=exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8
# recommended to not forget the labels
cp annotations/label_map.pbtxt exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model/
```

Test on an image:
```sh
cd /dl/detectors/training_tf2_vol
python3 scripts/detect_on_image.py --model=workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model --labels=workspace/pill_test/exported-models/pills_ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8/saved_model/label_map.pbtxt --image=workspace/pill_test/images/test/i-1e092ec6eabf47f9b85795a9e069181b.jpg --threshold=0.5
```

## Develop notes:

```sh
# install protobuf
apt install -y protobuf-compiler
# test protobuf
cd /dl/detectors/training_tf2_vol/packages/tensorflow_models/research
protoc object_detection/protos/*.proto --python_out=.

# install Object Detection API
cd /dl/detectors/training_tf2_vol/packages/tensorflow_models/research
cp object_detection/packages/tf2/setup.py .
python -m pip install --use-feature=2020-resolver .
apt-get update
apt-get install ffmpeg libsm6 libxext6 -y
# run tests for object detection API
python object_detection/builders/model_builder_tf2_test.py
```