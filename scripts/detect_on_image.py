import argparse
import os
import time
from abc import ABC, abstractmethod
from copy import deepcopy
from pprint import pprint

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Suppress TensorFlow logging
os.environ["CUDA_VISIBLE_DEVICES"] = '0'
import cv2
import numpy as np
import tensorflow as tf
import warnings
from object_detection.utils import label_map_util
warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings
tf.get_logger().setLevel('ERROR')


def parse_args():
    desc = ('Test TensorFlow 2 detector from saved_model.')
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-m', '--model',
                        help=('Path to the `saved_model`. Example: '
                              '"exported-models/my_model/saved_model"'),
                        type=str)
    parser.add_argument('-l', '--labels',
                        help='Path to the `label_map.pbtxt`.',
                        type=str)
    parser.add_argument('-i', '--image',
                        help='Filepath of an image for testing.',
                        type=str,
                        default='workspace/pill_test/images/test/i-1e092ec6eabf47f9b85795a9e069181b.jpg')
    parser.add_argument('-oi', '--ouput_image',
                        help='Filepath of the output image with drawn results',
                        type=str,
                        default='test_image.png')
    parser.add_argument('-th', '--threshold',
                        help='Detector threshold',
                        type=float,
                        default=0.5)
    args = parser.parse_args()
    return args


class DetectorInterface(ABC):

    @abstractmethod
    def detect(self, image):
        pass


class BaseDetector():

    def __init__(self, model_path: str, labels_path: str, threshold: float):
        self._model_path = model_path
        self._labels_path = labels_path
        self._threshold = threshold

    def _run_dummy_fp(self):
        _ = self.detect(image=(np.zeros((300, 300, 3), dtype=np.uint8)))

    def _preprocess(self, image):
        return image

    @abstractmethod
    def _process(self, network_input):
        """
        Returns
        -------
        detections: List[dict()]
            The detections in format:
            {
                'bbox': {
                    'xmin': int,
                    'ymin': int,
                    'xmax': int,
                    'ymax': int,
                },
                'score': float,  # in [0, 1]
                'class_id': int,
                'class_name': str,
            }
        """
        pass

    def _postprocess(self, predictions):
        return predictions

    def detect(self, image):
        network_input = self._preprocess(image=image)
        predictions = self._process(
            network_input=network_input,
            input_shape=image.shape,  # HWC
        )
        detections = self._postprocess(predictions=predictions)
        return detections


class TF2Detector(BaseDetector):
    _MAX_GPU_MEMORY_ALLOCATION = 2048

    def __init__(self, model_path: str, labels_path: str, threshold: float):
        super().__init__(
            model_path=model_path,
            labels_path=labels_path,
            threshold=threshold
        )
        self._allocate_gpu_memory(memory_limit=self._MAX_GPU_MEMORY_ALLOCATION)
        self._detection_fn = self._load_model()
        self._labels_mapper = self._load_labels()

    def _allocate_gpu_memory(self, memory_limit: int):
        # Disable GPU dynamic memory allocation
        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, False)
            # limit gpu memory
            tf.config.experimental.set_virtual_device_configuration(
                gpu,
                [
                    tf.config.experimental.VirtualDeviceConfiguration(
                        memory_limit=memory_limit
                    )
                ]
            )

    def _load_model(self):
        """
        Load saved model and build detection function.
        """
        return tf.saved_model.load(self._model_path)

    def _load_labels(self):
        return label_map_util.create_category_index_from_labelmap(
            self._labels_path,
            use_display_name=True
        )

    def _preprocess(self, image):
        # image_expanded = np.expand_dims(image, axis=0)
        input_tensor = tf.convert_to_tensor(image)
        # The model expects a batch of images, so add an axis with `tf.newaxis`
        input_tensor = input_tensor[tf.newaxis, ...]
        return input_tensor

    def _process(self, network_input, input_shape: tuple):
        """
        Returns
        -------
        detections: List[dict()]
            The detections in format:
            {
                'bbox': {
                    'xmin': int,
                    'ymin': int,
                    'xmax': int,
                    'ymax': int,
                },
                'score': float,  # in [0, 1]
                'class_id': int,
                'class_name': str,
            }
        """

        # return batches tensors
        raw_predictions = self._detection_fn(network_input)

        # Convert to numpy arrays, and take index [0] to remove the batch
        # dimension.
        # We're only interested in the first num_detections.
        num_detections = int(raw_predictions.pop('num_detections'))
        # for k, v in raw_predictions.items(): print(k, v.shape)
        detections = {
            output_layer: value[0, :num_detections].numpy()
            for output_layer, value in raw_predictions.items()
        }
        # detections['num_detections'] = num_detections
        scores = detections['detection_scores']  # [[y0, x0, y1, x1], ..]
        boxes = detections['detection_boxes']
        classes = detections['detection_classes'].astype(np.int8)

        detections = list()
        for i, score in enumerate(scores):
            if score < self._threshold:
                break

            # coordinates could be outside of image dimensions
            xmin = int(boxes[i][1] * input_shape[1])
            ymin = int(boxes[i][0] * input_shape[0])
            xmax = int(min(input_shape[1], (boxes[i][3] * input_shape[1])))
            ymax = int(min(input_shape[0], (boxes[i][2] * input_shape[0])))

            detections.append({
                'bbox': {
                    'xmin': xmin,
                    'ymin': ymin,
                    'xmax': xmax,
                    'ymax': ymax,
                },
                'score': round(score, 2),
                'class_id': classes[i],
                'class_name': self._labels_mapper[classes[i]]['name'],
            })
        return detections


class DetectionDrawer:
    _BBOXES_COLOR = (10, 200, 0)  # green
    _THICKNESS = 2
    _TEXT_FONT = cv2.FONT_HERSHEY_SIMPLEX
    _TEXT_SCALE = 0.7
    _TEXT_THINKNESS = 1
    _TEXT_COLOR = (250, 250, 250)  # white

    def draw_detections(self, image, detections):
        """
        Parameters
        -------
        detections: List[dict()]
            The detections in format:
            {
                'bbox': {
                    'xmin': int,
                    'ymin': int,
                    'xmax': int,
                    'ymax': int,
                },
                'score': float,  # in [0, 1]
                'class_id': int,
                'class_name': str,
            }
        """
        image_drawn = deepcopy(image)
        for det in detections:
            # draw bbox
            image_drawn = cv2.rectangle(
                image_drawn,
                (det['bbox']['xmin'], det['bbox']['ymin']),
                (det['bbox']['xmax'], det['bbox']['ymax']),
                self._BBOXES_COLOR,
                self._THICKNESS
            )

            # write label
            label = f"{det['class_name']}: {det['score']}"
            labelSize, baseLine = cv2.getTextSize(
                label,
                self._TEXT_FONT,
                self._TEXT_SCALE,
                self._TEXT_THINKNESS
            )
            label_ymin = max(det['bbox']['ymin'], labelSize[1] + 10)
            image_drawn = cv2.rectangle(
                image_drawn,
                (det['bbox']['xmin'], label_ymin - labelSize[1] - 10),
                (det['bbox']['xmin'] + labelSize[0], label_ymin + baseLine - 10),
                self._BBOXES_COLOR,
                cv2.FILLED
            )
            image_drawn = cv2.putText(
                image_drawn,
                label,
                (det['bbox']['xmin'], label_ymin - 7),
                self._TEXT_FONT,
                self._TEXT_SCALE,
                self._TEXT_COLOR,
                self._TEXT_THINKNESS
            )
        return image_drawn


def main():
    args = parse_args()
    model_repr = os.path.basename(args.model.split('/saved_model')[0])
    drawer = DetectionDrawer()

    print(f'Loading detector: {model_repr}')
    t0 = time.time()
    detector = TF2Detector(
        model_path=args.model,
        labels_path=args.labels,
        threshold=args.threshold,
    )
    print(f'Took: {(time.time() - t0) * 1000:.2f}ms')
    detector._run_dummy_fp()

    image = cv2.imread(args.image)
    rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    t1 = time.time()
    detections = detector.detect(image=rgb_image)
    print(f'Time for detection: {(time.time() - t1) * 1000:.2f}ms')

    image_drawn = drawer.draw_detections(image=image, detections=detections)

    print(f'Number of detections: {len(detections)}')
    pprint(detections)
    print(f'Result saved on: {args.ouput_image}')
    cv2.imwrite(args.ouput_image, image_drawn)


if __name__ == "__main__":
    main()
